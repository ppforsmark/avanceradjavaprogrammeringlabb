Översikt

Den här inlämningsuppgiften består i att göra nedanstående labbuppgifter. Du kan lägga koden för respektive uppgift i ett eget package. När du är klar publicerar du ditt resulterande projekt på Gitlab, och lämnar sedan in en länk till Gitlab-projektet i PingPong.

Uppgifter

Skapa en klass Person med fälten (variablerna):



private String name,

private String gender,

private double salary




Skapa en lista, och fyll den med 10 stycken sådana personer.



Med hjälp av streams, räkna ut:

1.1 Snittlönen för kvinnorna respektive männen i listan

1.2 Vem som har högst lön totalt

1.3 Vem som har lägst lön totalt.



2. Skapa en bilfabrik, med hjälp av factory pattern


3. Skapa en lista av ord. Använd reguljära uttryck för plocka ut endast de ord som innehåller 2 eller fler engelska vokaler (a, e, i, o, u, y)


4. Räkna ut antalet primtal inom intervallet 0 till 500'000. Dela upp intervallet på 2 eller flera trådar, som var för sig räknar på antalet primtal inom sin tilldelade del parallellt. Du kan t.ex. låta en tråd ta en första del (typ 0 till 350'000) och en annan tråd resten  350'001 till 500'000. Det är dock givetvis tillåtet med något eget mer avancerat och effektivt upplägg också. 