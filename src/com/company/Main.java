package com.company;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class Main {

    public static void main(String[] args) {

        //Creates a list of persons. Gender is standardized in the constructor
        List<Person> personList = new ArrayList<Person>(List.of(
                new Person("Alma", "Woman", 230000),
                new Person("Bertil", "man", 220000),
                new Person("Cino", "other", 210000),
                new Person("David", "nosay", 200000),
                new Person("Emilia", "woman", 223000),
                new Person("Julia", "woman", 120000),
                new Person("Richard", "man", 180000),
                new Person("Tuva", "other", 900000),
                new Person("Selma", "Woman", 500000),
                new Person("Johan", "MAN", 70000)
        ));

        //Prints all persons
        for (Person el:personList) {
            System.out.println(el);
        }

        //Print line for readability
        System.out.println();

        //Exercise 1.1
        //Print all woman
        personList.stream().filter(person -> person.getGender().equals("woman")).forEach(person -> System.out.println(person));
        //Get average of woman and print
        double averageSalaryW = personList.stream().filter(person -> person.getGender().equals("woman")).mapToDouble(person -> person.getSalary()).average().getAsDouble();
        System.out.println("Average salary for woman: " + averageSalaryW);

        //Print line for readability
        System.out.println();

        //Print all man
        personList.stream().filter(person -> person.getGender().equals("man")).forEach(person -> System.out.println(person));
        //Get average of men and print
        double averageSalaryM = personList.stream().filter(person -> person.getGender().equals("man")).mapToDouble(person -> person.getSalary()).average().getAsDouble();
        System.out.println("Average salary for man: " + averageSalaryM);

        //Print line for readability
        System.out.println();

        //Exercise 1.2
        //Sort persons based on salary. Reverse and print. 2 ways -> Store the person and limit list to 1
        Optional<Person> personHighest = personList.stream().sorted(Comparator.comparing(Person::getSalary).reversed()).findFirst();
        personList.stream().sorted(Comparator.comparing(Person::getSalary).reversed()).limit(1).forEach(person -> System.out.println(person));
        System.out.println("Highest salary: " + personHighest);

        //Exercise 1.3
        //Sort persons based on salary. Reverse and print first
        Optional<Person> personLowest = personList.stream().sorted(Comparator.comparing(Person::getSalary)).findFirst();
        System.out.println("Lowest salary: " + personLowest);


    }
}
