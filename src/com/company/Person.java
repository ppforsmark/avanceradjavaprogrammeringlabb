package com.company;

public class Person {
    private String name;
    private String gender;
    private double salary;

    public Person(String nameParam, String genderParam, double salaryParam){
        this.name = nameParam;
        this.salary = salaryParam;

        if(genderParam.toLowerCase().equals("man")){
            this.gender = "man";
        }else if(genderParam.toLowerCase().equals("woman")){
            this.gender = "woman";
        }else{
            this.gender = "other";
        }
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public double getSalary() {
        return salary;
    }

    public String toString(){
        String aString = "";
        aString = this.name + " : " + this.gender + " : " + this.salary;
        return aString;
    }


}
