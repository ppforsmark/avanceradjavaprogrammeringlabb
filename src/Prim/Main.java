package Prim;

//To get access of the static array without creating objects we need to import it to this document.
import static Prim.Main.stIntArr;

public class Main {

    //Will store all our values. Uses a static array and send in the index of the Integer the different threads shall manipulate to prevent
    //that multiple threads work at the same var and still doesn't have to wait for each other with synchronized.
    public static Integer[] stIntArr = new Integer[]{
      0, 0, 0, 0, 0
    };

    public static void main(String[] args) throws InterruptedException {

        //Prim are greater than 1 therefore well start testing at 2
        IsPrim isPrim0 = new IsPrim(2, 100000, 0);
        Thread thread0 = new Thread(isPrim0);
        IsPrim isPrim1 = new IsPrim(100001, 200000, 1);
        Thread thread1 = new Thread(isPrim1);
        IsPrim isPrim2 = new IsPrim(200001, 300000, 2);
        Thread thread2 = new Thread(isPrim2);
        IsPrim isPrim3 = new IsPrim(300001, 400000, 3);
        Thread thread3 = new Thread(isPrim3);
        IsPrim isPrim4 = new IsPrim(400001, 500000, 4);
        Thread thread4 = new Thread(isPrim4);

        Long startTime = System.nanoTime();

        thread0.start();
        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
        thread0.join();
        thread1.join();
        thread2.join();
        thread3.join();
        thread4.join();

        Long endTime = System.nanoTime();

        System.out.println("NRofPrims all: " + (stIntArr[0] + stIntArr[1] + stIntArr[2] + stIntArr[3] + stIntArr[4]));
        System.out.println("NRofPrims T1: " + stIntArr[0]);
        System.out.println("NRofPrims T2: " + stIntArr[1]);
        System.out.println("NRofPrims T3: " + stIntArr[2]);
        System.out.println("NRofPrims T4: " + stIntArr[3]);
        System.out.println("NRofPrims T5: " + stIntArr[4]);

        System.out.println("------------");
        System.out.println("Nano: " + (endTime - startTime));
    }
}

class IsPrim implements Runnable{

    int start;
    int end;
    int saveIndex;

    public IsPrim(int startParam, int endParam, int saveIndex){
        this.start = startParam;
        this.end = endParam;
        this.saveIndex = saveIndex;
    }

    @Override
    public void run() {
        for(int i = start; i <= end; i++){
            boolean isPrim = true;

            //<= because first input is 2 and will need to be tested
            for(int j = 2; j <= i; j++){
                    if (i % j == 0 && j!=i) {
                        isPrim = false;
                        break;
                    }
            }
            if(isPrim) {
                stIntArr[saveIndex]++;
            }
        }
    }
}

