package RegEx;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        List<String> wordList = new ArrayList<String>();
        wordList.add("hello");
        wordList.add("yesterday");
        wordList.add("monday");
        wordList.add("likeable");
        wordList.add("discourage");
        wordList.add("timeline");
        wordList.add("regular");
        wordList.add("expression");
        wordList.add("surname");
        wordList.add("SPACE");
        wordList.add("GrATitude");
        wordList.add("shIpWreck");
        wordList.add("no");
        wordList.add("hint");
        wordList.add("yes");

        //All but two will match, no and hint doesn't match.

        //Match case-insensitive a,e,i,o,u or y. Then match any character 0 or more times and try for a,e,i,o,u or y again. That means that
        //if a,e,i,o,u or y are found 2 or more times a match will be true
        Pattern pattern = Pattern.compile("(?i)([aeiouy].*){2,}");

        System.out.println("---1---");
        System.out.println();

        wordList.stream().filter(pattern.asPredicate()).collect(Collectors.toList()).forEach(word -> System.out.println(word));

        System.out.println();
        System.out.println("---2---");
        System.out.println();

        for (String word: wordList) {
            if(pattern.matcher(word).find()){
                System.out.println("True-> " + word);
            }else{
                System.out.println("False-> " + word);
            }
        }

        System.out.println();
        System.out.println("---3---");
        System.out.println();


        wordList.stream().filter(word -> (pattern.matcher(word).find())).forEach(System.out::println);

        //wordList.stream().map(word -> word.toLowerCase()).filter(word -> word.contains([aeiouy]{2,})).forEach(word -> System.out.println(word));
        //.filter(word -> word.contains((?i)[aeiouy]{2,})).forEach(word -> System.out.println(word));

    }
}
