package CarFactory;

public class Volvo extends Car {

    public Volvo(String brand, String desc) {
        super(brand, desc);
    }
}
