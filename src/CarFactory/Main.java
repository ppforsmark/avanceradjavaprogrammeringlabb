package CarFactory;

public class Main {

    public static void main(String[] args) {

        CarFactory carFactory = new CarFactory();

        Car carOne = carFactory.createCar(CarType.FERRARI);
        Car carTwo = carFactory.createCar(CarType.VOLVO);
        Car carThree = carFactory.createCar(CarType.PORCHE);

        System.out.println("--------");
        System.out.println(carOne.whatIsThis());
        System.out.println("--------");
        System.out.println(carTwo.whatIsThis());
        System.out.println("--------");
        System.out.println(carThree.whatIsThis());

    }
}
