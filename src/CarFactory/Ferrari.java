package CarFactory;

public class Ferrari extends Car {

    public Ferrari(String brand, String desc) {
        super(brand, desc);
    }
}
