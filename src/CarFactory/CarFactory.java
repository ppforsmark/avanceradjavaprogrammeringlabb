package CarFactory;

public class CarFactory {

    public Car createCar(CarType carType){
        return switch(carType){
            case VOLVO -> new Volvo("Volvo", "A safe but boring car");
            case FERRARI -> new Ferrari("Ferrari", "Fasten your seatbelts, this will go fast");
            case PORCHE -> new Porche("Porsche", "Thinks its cool but....naah");
        };
    }
}
