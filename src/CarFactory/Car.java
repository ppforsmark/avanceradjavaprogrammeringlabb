package CarFactory;

abstract public class Car {

    String brand;
    String desc;

    public Car(String brand, String desc) {
        this.brand = brand;
        this.desc = desc;
    }

    public String whatIsThis(){
        return String.format("This is a %s! %s", brand, desc);
    }
}
