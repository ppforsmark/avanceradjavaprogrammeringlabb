package CarFactory;

public enum CarType {
    VOLVO,
    PORCHE,
    FERRARI
}
